package net.toomuchram.backendDummy.managers

import com.natpryce.konfig.*
import java.io.File
import kotlin.system.exitProcess

class ConfigManager {

    private val config: Configuration


    /**
     * Function to read the database config from the applicationConfig.properties
     * @return The database configuration, containing the username, password, host and database
     */
    fun readDatabaseConfig(): DatabaseConfig {
        val mysqlUsername = Key("mysql.username", stringType)
        val mysqlPassword = Key("mysql.password", stringType)
        val mysqlHost = Key("mysql.host", stringType)
        val mysqlDatabase = Key("mysql.database", stringType)

        return DatabaseConfig(
            config[mysqlUsername],
            config[mysqlPassword],
            config[mysqlHost],
            config[mysqlDatabase]
        )
    }

    /**
     * Function that reads the `registration.unsafe` value
     * @return `registration.unsafe` config value
     */
    fun readRegistrationSafety(): Boolean {
        val registrationSafety = Key("registration.unsafe", booleanType)

        return config[registrationSafety]
    }

    /**
     * Function that reads configuration in the fcm category
     * @return The FCM configuration
     */
    fun readFcmConfig(): FCMConfig {
        val fcmApiKey = Key("fcm.apikey", stringType)
        val fcmAnnouncementTitle = Key("fcm.announcementTitle", stringType)
        val fcmAnnouncementSound = Key("fcm.announcementSound", stringType)
        val fcmPackageName = Key("fcm.packagename", stringType)


        return FCMConfig(
            config[fcmApiKey],
            config[fcmAnnouncementTitle],
            config[fcmAnnouncementSound],
            config[fcmPackageName]
        )
    }

    /**
     * Function that reads the APNs-related settings
     * @return The APNs configuration
     */
    fun readAPNsConfig(): APNsConfig {
        val apnsCertificatePath = Key("apns.certificatePath", stringType)
        val apnsCertificateKey = Key("apns.certificateKey", stringType)
        val apnsAnnouncementTitle = Key("apns.announcementTitle", stringType)
        val apnsAnnouncementSound = Key("apns.announcementSound", stringType)
        val apnsPackageName = Key("apns.packagename", stringType)
        val apnsProduction = Key("apns.production", booleanType)

        return APNsConfig(
            config[apnsCertificatePath],
            config[apnsCertificateKey],
            config[apnsAnnouncementTitle],
            config[apnsAnnouncementSound],
            config[apnsPackageName],
            config[apnsProduction]
        )
    }

    /**
     * Function to read the port ktor should run on and the domain name from which the service is accessible
     * @return The port and the domain
     */
    fun readWebserverConfig(): WebserverConfig {
        val ktorPort = Key("webserver.port", intType)
        val domain = Key("webserver.domain", stringType)
        val cors = Key("webserver.cors", booleanType)
        return WebserverConfig(
            config[ktorPort],
            config[domain],
            config[cors]
        )
    }

    fun readWebSocketPort(): Int {
        val wsPort = Key("websocket.port", intType)
        return config[wsPort]
    }

    init {
        try {
            config = ConfigurationProperties.systemProperties() overriding
                    EnvironmentVariables() overriding
                    ConfigurationProperties.fromFile(File("applicationConfig.properties"))
        } catch (e: Misconfiguration) {
            println("ERROR: Could not read config")
            println("Make sure applicationConfig.properties is present in the current directory")
            println("Stacktrace:")
            e.printStackTrace()
            exitProcess(-1)
        }
    }

}
