package net.toomuchram.backendDummy.managers

import io.ktor.http.*
import org.joda.time.DateTime
import java.sql.Blob

data class Announcement(
    val notiftime: String,
    val message: String,
    val username: String
)
data class ChatMessage(
    var id: Int = 0, /* id is optional */
    val user: String,
    val timestamp: DateTime,
    val img: Blob,
    val contentType: ContentType?,
    val message: String
)


data class DatabaseConfig(
    val username: String,
    val password: String,
    val host: String,
    val database: String
)

data class FCMConfig(
    val apikey: String,
    val announcementTitle: String,
    val announcementSound: String,
    val packageName: String
)

data class APNsConfig(
    val certificatePath: String,
    val certificateKey: String,
    val announcementTitle: String,
    val announcementSound: String,
    val packageName: String,
    val production: Boolean
)

data class WebserverConfig(
    val port: Int,
    val domain: String,
    val cors: Boolean
)

data class User(
    val username: String,
    val password: String,
    val excursion: Int
)

data class Team (
    val id: Int,
    val name: String,
    val category: Int
)

data class Excursion(
    val id: Int,
    val name: String
)