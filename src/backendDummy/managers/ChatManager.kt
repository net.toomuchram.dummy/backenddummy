package net.toomuchram.backendDummy.managers

import io.ktor.http.ContentType
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import javax.sql.rowset.serial.SerialBlob


class ChatManager {


    fun addMessage(message: ChatMessage, teamId: Int?, excursionId: Int): ChatMessage {
        val contentType = if (message.contentType != null) {
            message.contentType.toString()
        } else {
            ""
        }

        val teamEntityId = teamId?.let { EntityIDConverter.getTeamEntityID(it) }

        val messageId: Int = if (teamId != null) {
            transaction {
                return@transaction Tables.Msg.select {
                    Tables.Msg.teamId eq teamEntityId
                }.count() + 1
            }
        } else {
            transaction {
                return@transaction Tables.Msg.select {
                    Tables.Msg.teamId.isNull() and (Tables.Msg.excursion eq excursionId)
                }.count() + 1
            }
        }

        val excursionEntityID = EntityIDConverter.getExcursionEntityID(excursionId)
        val userId: EntityID<Int> = transaction {
            val users = Tables.Users.slice(Tables.Users.id).select {
                Tables.Users.username eq message.user and
                        (Tables.Users.excursion eq excursionId)
            }

            if (users.empty()) {
                throw Exceptions.NoSuchUserException("No such user found")
            }
            return@transaction users.elementAt(0)[Tables.Users.id]
        }

        transaction {
            return@transaction Tables.Msg.insert {
                it[Tables.Msg.messageId] = messageId
                it[Tables.Msg.teamId] = teamEntityId
                it[timestamp] = message.timestamp
                it[img] = message.img
                it[Tables.Msg.contentType] = contentType
                it[user] = userId
                it[Tables.Msg.message] = message.message
                it[excursion] = excursionEntityID
            }
        }
        message.id = messageId
        return message
    }

    fun getAllMessages(
        batch: Int = 0,
        teamId: Int?,
        excursionId: Int
    ): List<ChatMessage> {
        return transaction {
            val allMsgs = mutableListOf<ChatMessage>()

            val teamEntityId = teamId?.let { EntityIDConverter.getTeamEntityID(it) }

            val query = Tables.Msg.select {
                Tables.Msg.teamId eq teamEntityId and
                        (Tables.Msg.excursion eq excursionId)
            }
            if (query.count() == 0) {
                allMsgs.add(
                    ChatMessage(
                        id = 1,
                        user = " ",
                        timestamp = DateTime.now(),
                        img = SerialBlob(byteArrayOf(0)),
                        contentType = null,
                        message = "No messages sent yet"
                    )
                )
            } else {
                query.limit(25, query.count()-(batch+1)*25)
                    .forEach {
                        val userId = it[Tables.Msg.user]
                        val username = getUsername(userId?.value)

                    allMsgs.add(
                        ChatMessage(
                            id = it[Tables.Msg.messageId],
                            user = username,
                            timestamp = it[Tables.Msg.timestamp],
                            img = it[Tables.Msg.img],
                            contentType = convertStringToContentType(it[Tables.Msg.contentType]),
                            message = it[Tables.Msg.message]
                        )
                    )
                }
            }
            return@transaction allMsgs
        }
    }


    fun getMessage(messageId: Int, teamId: Int?, excursionId: Int): ChatMessage? {
        return transaction {
            val teamEntityId = teamId?.let { EntityIDConverter.getTeamEntityID(it) }
            val msgs = Tables.Msg.select {
                (Tables.Msg.teamId eq teamEntityId) and
                        (Tables.Msg.messageId eq messageId) and
                        (Tables.Msg.excursion eq excursionId)
            }


            return@transaction if (msgs.count() > 0) {
                val msg = msgs.elementAt(0)
                val userId = msg[Tables.Msg.user]
                val username = getUsername(userId?.value)
                ChatMessage(
                    id = msg[Tables.Msg.messageId],
                    user = username,
                    timestamp = msg[Tables.Msg.timestamp],
                    img = msg[Tables.Msg.img],
                    contentType = convertStringToContentType(msg[Tables.Msg.contentType]),
                    message = msg[Tables.Msg.message]
                )
            } else {
                null
            }
        }
    }

    /**
     * Function to delete all messages sent in a particular team chat
     * @param teamId The ID of the team
     */
    fun deleteAllMessages(teamId: Int?, excursionId: Int) {
        val teamEntityId = teamId?.let { EntityIDConverter.getTeamEntityID(it) }
        transaction {
            Tables.Msg.deleteWhere { Tables.Msg.teamId eq teamEntityId and (Tables.Msg.excursion eq excursionId) }
        }
    }

    /**
     * Function to convert a content type string (eg. "image/png") to a ktor ContentType
     */
    private fun convertStringToContentType(contentType: String): ContentType? {
        return if (contentType != "") {
            ContentType(
                contentType = contentType.split("/")[0],
                contentSubtype = contentType.split("/")[1]
            )
        } else {
            null
        }
    }

    private fun getUsername(userId: Int?): String {
        return if (userId != null) {
            Tables.Users.slice(Tables.Users.username).select {
                Tables.Users.id eq userId
            }.elementAt(0)[Tables.Users.username]
        } else {
            "[DELETED]"
        }
    }
/*
    *//**
     * Function to send a chat notification to all admins
     * Used for the admin chat
     *//*
    fun sendNotificationToAdmins(title: String, message: String, currentuser: Int, excursionId: Int) {
        val admins = userManager.getAllAdmins(excursionId)
        // This is to prevent the user from getting their own messages
        val adminsWithoutCurrentUser = admins.toMutableList()
        adminsWithoutCurrentUser.remove(currentuser)
        val tokenList = announcementManager.getDeviceTokensForUsers(adminsWithoutCurrentUser)
        tokenList.map { (token, devicetype) ->
            CoroutineScope(Dispatchers.IO).launch {
                if (devicetype == "iOS") {
                    sendiOSChatNotification(title, message, token, "admin")
                } else if (devicetype == "Android") {
                    sendAndroidChatNotification(title, message, token, "admin")
                }
            }
        }
    }

    *//**
     * Function to send a chat notification to iOS devices
     * @param title The title of the notification
     * @param message The content of the notification
     * @param token The device to which to send the notification
     * @param chatType The type of chat, which is attached in a custom property called "chat"
     *//*
    private suspend fun sendiOSChatNotification(title: String, message: String, token: String, chatType: String) {
        withContext(Dispatchers.IO) {
            val config = configManager.readAPNsConfig()
            val payloadBuilder = ApnsPayloadBuilder()
            payloadBuilder.setAlertTitle(title)
            payloadBuilder.setAlertBody(message)
            payloadBuilder.addCustomProperty("chat", chatType)
            val payload = payloadBuilder.buildWithDefaultMaximumLength()


            val pushNotification = SimpleApnsPushNotification(token, config.packageName, payload)
            val sendNotificationFuture = announcementManager.apnsClient.sendNotification(pushNotification)
            // Add the announcement manager APNs callback, which will handle cleaning up the invalid device tokens
            sendNotificationFuture.addListener(AnnouncementManager.APNsCallback())
        }
    }

    *//**
     * Function to send a chat notification to Android devices
     * @param title The title of the notification
     * @param message The content of the notification
     * @param token The device to which to send the notification
     * @param chatType The type of chat, which is attached in a custom property called "chat"
     *//*
    private suspend fun sendAndroidChatNotification(
        title: String,
        message: String,
        token: String,
        chatType: String
    ) {
        withContext(Dispatchers.IO) {
            val config = configManager.readFcmConfig()
            val apikey = config.apikey
            val packageName = config.packageName

            val sender = Sender(apikey)

            val notification = Notification(
                title,
                message
            )

            val fcmMessage = Message.MessageBuilder()
                .toToken(token)
                .notification(notification)
                .priority(Message.Priority.NORMAL)
                .restrictedPackageName(packageName)
                .addData("chat", chatType)
                .build()

            sender.send(fcmMessage, AnnouncementManager.FCMCallback(token))
        }
    }*/
}

