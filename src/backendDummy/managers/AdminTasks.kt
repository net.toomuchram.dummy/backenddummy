package net.toomuchram.backendDummy.managers

import net.toomuchram.backendDummy.responders.UserResponse
import net.toomuchram.backendDummy.Hasher
import org.jetbrains.exposed.sql.*

import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

class AdminTasks(private val sessionManager: SessionManager, private val announcementManager: AnnouncementManager) {
    private val userDatabase = Tables.Users

    /**
     * All of these are really simple database manipulation functions
     * You can figure out yourself what they do
     */
    fun getAllUsers(excursionId: Int): List<UserResponse> {
        return transaction {
            val users = mutableListOf<UserResponse>()
            userDatabase.slice(userDatabase.id, userDatabase.username, userDatabase.admin).select{ userDatabase.excursion eq excursionId }.map {
                users.add(
                    UserResponse(
                        id = it[userDatabase.id].value,
                        username = it[userDatabase.username],
                        admin = it[userDatabase.admin]
                    )
                )
            }
            return@transaction users
        }
    }

    fun makeAdmin(userId: Int, excursionId: Int) {
        transaction {
            userDatabase.update({
                (userDatabase.id eq userId) and (Tables.Users.excursion eq excursionId)
            }) {
                it[userDatabase.admin] = true
            }
        }
    }
    fun makeNormalUser(userId: Int, excursionId: Int) {
        transaction {
            userDatabase.update({
                (userDatabase.id eq userId) and (Tables.Users.excursion eq excursionId)
            }) {
                it[userDatabase.admin] = false
            }
        }
    }

    fun deleteUser(userId: Int, excursionId: Int) {
        try {
            announcementManager.deleteAllDeviceTokens(userId)
        } finally {
            transaction {
                Tables.Users.deleteWhere {
                    (Tables.Users.id eq userId) and (Tables.Users.excursion eq excursionId)
                }
            }
            sessionManager.endAllSessions(userId)
        }
    }

    fun changePassword(userId: Int, newPassword: String, excursionId: Int) {
        val hashedPassword = BCryptPasswordEncoder().encode(Hasher().sha512(newPassword))
        transaction {
            userDatabase.update({
                (userDatabase.id eq userId) and (userDatabase.excursion eq excursionId)
            }) {
                it[userDatabase.password] = hashedPassword
            }
        }
    }
}