package net.toomuchram.backendDummy.websocket

// General incoming message
data class IncomingWebsocketMessage(
    val channel: String?,
    val sessionId: String?,
    val lat: Double?,
    val lon: Double?
)

// Response message for channel errors
data class ErrorMessage(
    val channel: String = "errors",
    val error: String
)

// Response message for channel authentication
data class AuthenticationMessage(
    val channel: String = "authentication",
    val message: String
)

// Message for chat message
data class WSChatMessage(
    var channel: String = "msg",
    val id: Int,
    var imgpath: String,
    val timestamp: String,
    val user: String,
    val message: String,
    val teamId: String/*Can also be "globalchat"*/
)