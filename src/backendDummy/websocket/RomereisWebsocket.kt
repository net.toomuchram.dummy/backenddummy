package net.toomuchram.backendDummy.websocket

import net.toomuchram.backendDummy.websocket.AuthenticationMessage
import net.toomuchram.backendDummy.websocket.ErrorMessage
import net.toomuchram.backendDummy.websocket.IncomingWebsocketMessage
import net.toomuchram.backendDummy.websocket.WSChatMessage
import net.toomuchram.backendDummy.managers.SessionManager
import com.google.gson.Gson
import com.google.gson.JsonParseException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.toomuchram.backendDummy.managers.TeamsManager
import net.toomuchram.backendDummy.managers.UserManager
import org.java_websocket.WebSocket
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.InetSocketAddress
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


class RomereisWebsocket (
    address: InetSocketAddress,
    private val sessionRegistry: WebsocketSessionRegistry,
    private val sessionManager: SessionManager,
    private val userManager: UserManager,
    private val locationTracker: LocationTracker,
    private val teamsManager: TeamsManager
): WebSocketServer(address) {

    private val gson = Gson()
    private val logger: Logger = LoggerFactory.getLogger(RomereisWebsocket::class.java)

    override fun broadcast(text: String?) {
        throw Exception("Please do not use this method!")
    }
    fun broadcast(text: String?, excursion: Int) {
        sessionRegistry.sessions.forEach { (webSocket, metadata) ->
            if (metadata.excursionId == excursion) {
                webSocket.send(text)
            }
        }
    }

    override fun onOpen(conn: WebSocket, handshake: ClientHandshake) {

    }

    override fun onClose(conn: WebSocket, code: Int, reason: String, remote: Boolean) {
        sessionRegistry.removeSession(conn)
    }

    override fun onMessage(conn: WebSocket, message: String) {
        CoroutineScope(Dispatchers.IO).launch {
            logger.trace("Received websocket message: $message")

            try {
                val parsedMessage = gson.fromJson(message, IncomingWebsocketMessage::class.java)
                if (parsedMessage.channel == null) {
                    val errorMsg = ErrorMessage(error = "ERR_MISSING_CHANNEL")
                    conn.send(gson.toJson(errorMsg))
                    return@launch
                }
                if (parsedMessage.sessionId == null) {
                    val errorMsg = ErrorMessage(error = "ERR_MISSING_SESSIONID")
                    conn.send(gson.toJson(errorMsg))
                    return@launch
                } else if (!sessionManager.verifySession(parsedMessage.sessionId)) {
                    val errorMsg = ErrorMessage(error = "ERR_NO_SUCH_SESSION")
                    conn.send(gson.toJson(errorMsg))
                    conn.close()
                    return@launch
                }

                when (parsedMessage.channel) {
                    "authentication" -> {
                        authenticate(conn, parsedMessage)
                    }
                    "locations" -> {
                        broadcastLocation(conn, parsedMessage)
                    }
                    "getCachedLocations" -> {
                        val userId = sessionManager.getUserId(parsedMessage.sessionId)
                        val excursionId = userManager.getExcursion(userId)
                        val cachedLocations = locationTracker.getAllLocations(excursionId)
                        for(cachedLocation in cachedLocations) {
                            conn.send(gson.toJson(cachedLocation))
                        }
                    }
                }
            } catch (e: JsonParseException) {
                val errorMessage = ErrorMessage(error = "ERR_MALFORMED_MESSAGE")
                conn.send(gson.toJson(errorMessage))
            }
        }

    }

    override fun onError(conn: WebSocket?, ex: java.lang.Exception) {
        ex.printStackTrace()
    }

    override fun onStart() {
        logger.info("WebSocket service running at ws://${address.hostName}:${address.port}")
    }

    /**
     * Broadcast a chat message to all members of an excursion
     * @param excursion The excursion to send the message to
     * @param message A lambda which is given a session ID as an argument and returns a Chat Message, that is sent
     */
    fun broadcastChatMessage(excursion: Int, message: (String) -> WSChatMessage) {
        for ((conn, metadata) in sessionRegistry.sessions) {
            if (metadata.excursionId == excursion) {
                conn.send(gson.toJson(message(metadata.sessionId)))
            }
        }
    }

    fun broadcastChatMessage(excursion: Int, teamId: Int, message: (String) -> WSChatMessage) {
        for ((conn, metadata) in sessionRegistry.sessions) {
            if (metadata.excursionId == excursion && teamsManager.isUserInTeam(metadata.userId, teamId)) {
                conn.send(gson.toJson(message(metadata.sessionId)))
            }
        }
    }

    private fun authenticate(conn: WebSocket, message: IncomingWebsocketMessage) {
        CoroutineScope(Dispatchers.IO).launch {
            val userId = sessionManager.getUserId(message.sessionId!!)
            val excursionId = userManager.getExcursion(userId)
            sessionRegistry.addSession(
                conn,
                WSSessionMetadata(
                    userId,
                    message.sessionId,
                    excursionId
                )
            )
        }

        val successMessage = AuthenticationMessage(message = "AUTH_SUCCESS")
        conn.send(gson.toJson(successMessage))
    }

    private fun broadcastLocation(conn: WebSocket, message: IncomingWebsocketMessage) {

        if (message.lat == null || message.lon == null) {
            val errorMessage = ErrorMessage(error = "ERR_MISSING_COORDINATES")
            conn.send(gson.toJson(errorMessage))
            return
        }

        // No need to do this in a try/catch block, since the sessionId has already been checked
        val userId = sessionManager.getUserId(message.sessionId!!)
        val excursionId = userManager.getExcursion(userId)
        val username = userManager.getUsername(userId)

        val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
        val location = Location(
            username = username,
            lat = message.lat,
            lon = message.lon,
            time = ZonedDateTime.now().toOffsetDateTime().format(formatter)
        )
        locationTracker.addLocation(
            userId,
            excursionId,
            location
        )

        CoroutineScope(Dispatchers.IO).launch {
            val wsLocationBroadcast = gson.toJson(location)
            broadcast(wsLocationBroadcast, excursionId)
        }
    }
}