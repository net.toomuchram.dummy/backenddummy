package net.toomuchram.backendDummy.responders

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import net.toomuchram.backendDummy.managers.Exceptions
import net.toomuchram.backendDummy.responders.Managers

fun Routing.sessions(managers: Managers) {
    get("/getUsername") {
        val queryParameters = call.request.queryParameters

        val sessionId = queryParameters["sessionId"]!!

        try {
            val userId = if (queryParameters.contains("userId")) {
                queryParameters["userId"]!!.toInt()
            } else {
                managers.sessionManager.getUserId(sessionId)
            }
            call.respondText(managers.userManager.getUsername(userId))
        } catch (e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.NotFound, "ERR_NO_SUCH_SESSION")
        } catch(e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.NotFound, "ERR_NO_SUCH_USER")
        }
    }

    get("/verifySession") {
        val queryParameters = call.request.queryParameters

        if(!queryParameters.contains("sessionId")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_SESSIONID")
            return@get finish()
        }

        val sessionId = call.request.queryParameters["sessionId"]!!

        call.respond(managers.sessionManager.verifySession(sessionId).toString())
    }

    get("/isAdmin") {
        val queryParameters = call.request.queryParameters
        val sessionId = queryParameters["sessionId"]!!

        try {
            val userId = managers.sessionManager.getUserId(sessionId)

            call.respond(managers.userManager.isAdmin(userId))
        } catch (e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
        } catch (e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
        }
    }

    get("/isTeamMemberOf") {
        val queryParameters = call.request.queryParameters
        val sessionId = queryParameters["sessionId"]!!

        if(!queryParameters.contains("teamId")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_SESSIONID")
            return@get finish()
        }

        val teamId = queryParameters["teamId"]!!.toInt()

        try {
            val userId = managers.sessionManager.getUserId(sessionId)

            call.respond(managers.teamsManager.isUserInTeam(userId, teamId))
        } catch (e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
        } catch (e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
        }
    }
}