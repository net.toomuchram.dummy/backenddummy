package net.toomuchram.backendDummy.responders

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import net.toomuchram.backendDummy.managers.Exceptions
import net.toomuchram.backendDummy.responders.Helpers
import net.toomuchram.backendDummy.responders.Managers


fun Routing.announcements(
    managers: Managers,
    helpers: Helpers
) {
    /**
     * Endpoint for registering a device token in the database
     */
    post("/postDeviceToken/{devicetype}") {
        val parameters = call.parameters
        if (!parameters.contains("devicetype")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_INVALID_DEVICE_TYPE")
            return@post finish()
        }
        val deviceType = parameters["devicetype"]!!
        if (deviceType != "Android" && deviceType != "iOS") {
            call.respond(HttpStatusCode.BadRequest, "ERR_INVALID_DEVICE_TYPE")
            return@post finish()
        }

        val postParameters: Parameters = call.receiveParameters()
        if(!postParameters.contains("token")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_TOKEN")
            return@post finish()
        }

        val sessionId = postParameters["sessionId"]!!
        val token = postParameters["token"]!!

        // No need to check the session ID validity, that's done in the try/catch block
        // in the managers.sessionManager.getUserId function
        try {
            val userId = managers.sessionManager.getUserId(sessionId)
            managers.announcementManager.registerDeviceToken(
                userId = userId,
                devicetype = deviceType,
                token = token,
                sessionId = sessionId
            )
            call.respondText("SUCCESS")
        } catch(e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
        } catch(e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
        } catch(e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Endpoint for sending an announcement to all who have the app
     */
    post("/sendNotification") {
        val postParameters: Parameters = call.receiveParameters()
        if(!postParameters.contains("message")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_MESSAGE")
        }
        val sessionId = postParameters["sessionId"]!!
        val excursionId = helpers.excursionIdFromSession(sessionId)
        val message = postParameters["message"]!!

        if(message.length > 255) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MESSAGE_TOO_LONG")
            return@post finish()
        } else if (message == "") {
            call.respond(HttpStatusCode.BadRequest, "ERR_NO_MESSAGE")
            return@post finish()
        }
        try {
            val userId = managers.sessionManager.getUserId(sessionId)
            val username = managers.userManager.getUsername(userId)

            if(!managers.userManager.isAdmin(userId)) {
                call.respond(HttpStatusCode.Unauthorized, "ERR_UNAUTHORISED")
                return@post finish()
            }

            managers.announcementManager.sendNotification(message, username, excursionId)

            call.respondText("SUCCESS")
        } catch(e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
        } catch(e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
        } catch(e: Exception) {
            e.printStackTrace()
        }
    }

    get("/getAnnouncements") {
        val sessionId = call.parameters["sessionId"]!!
        val excursionId = managers.userManager.getExcursion(
            managers.sessionManager.getUserId(sessionId)
        )
        call.respond(managers.announcementManager.getAllAnnouncements(excursionId))
    }
}