package net.toomuchram.backendDummy.responders

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.post
import net.toomuchram.backendDummy.managers.*

fun Routing.login(managers: Managers, helpers: Helpers) {

    /**
     * Endpoint for ending a session
     */
    post("/logout") {
        val postParameters: Parameters = call.receiveParameters()

        val sessionId: String
        if(postParameters.contains("sessionId")) {
            sessionId = postParameters["sessionId"]!!
        } else {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_VARIABLES")
            return@post finish()
        }

        managers.sessionManager.endSession(sessionId)
        call.respondText("SUCCESS")
    }

    /**
     * Endpoint for starting a session
     */
    post("/login") {

        val postParameters: Parameters = call.receiveParameters()
        val (parametersCheckSuccess, parametersCheckErrorMessage) = helpers.checkPostParametersForUsernameAndPassword(
            postParameters
        )
        if (!parametersCheckSuccess) {
            call.respond(HttpStatusCode.BadRequest, parametersCheckErrorMessage)
            return@post finish()
        }

        val username = postParameters["username"]!!
        val password = postParameters["password"]!!

        if (!postParameters.contains("excursionId")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_NO_EXCURSION")
            return@post finish()
        }

        val excursionId = postParameters["excursionId"]!!.toInt()

        try {
            val uid = managers.userManager.login(
                User(
                    username,
                    password,
                    excursionId
                )
            )
            val isAdmin = managers.userManager.isAdmin(uid)
            val sessionId = managers.sessionManager.createSession(uid)

            val response = if (isAdmin) {
                LoginResponse(username, uid, sessionId, 1)
            } else {
                LoginResponse(username, uid, sessionId, 0)
            }
            call.respond(response)
        } catch (e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
        } catch (e: Exceptions.WrongPasswordException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_WRONG_PASSWORD")
        }
    }

    /**
     * Endpoint for registering a user in the database
     */
    post("/register") {
        val unsafe = managers.configManager.readRegistrationSafety()

        val postParameters: Parameters = call.receiveParameters()
        val (parametersCheckSuccess, parametersCheckErrorMessage) = helpers.checkPostParametersForUsernameAndPassword(
            postParameters
        )
        if (!parametersCheckSuccess) {
            call.respond(HttpStatusCode.BadRequest, parametersCheckErrorMessage)
            return@post finish()
        }

        val username = postParameters["username"]!!
        val password = postParameters["password"]!!


        if (!unsafe) {
            if (!postParameters.contains("sessionId")) {
                call.respond(HttpStatusCode.Unauthorized, "ERR_MISSING_SESSIONID")
                return@post finish()
            } else {
                val sessionId = postParameters["sessionId"]!!
                if (!managers.sessionManager.verifySession(sessionId)) {
                    call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
                    return@post finish()
                }
                val executingUserId = managers.sessionManager.getUserId(sessionId)
                if (!managers.userManager.isAdmin(executingUserId)) {
                    call.respond(HttpStatusCode.Unauthorized, "ERR_UNAUTHORISED")
                    return@post finish()
                }
            }
        }


        val excursionId = if (!unsafe) {
            val sessionId = postParameters["sessionId"]!!
            helpers.excursionIdFromSession(sessionId)
        } else {
            if (!postParameters.contains("excursionId")) {
                call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_EXCURSIONID")
                return@post finish()
            }
            postParameters["excursionId"]!!.toInt()
        }

        val admin: Boolean = if (postParameters.contains("admin")) {
            postParameters["admin"]!!.toBoolean()
        } else {
            false
        }

        try {
            managers.userManager.register(
                User(
                    username,
                    password,
                    excursionId
                ),
                admin
            )
            call.respond(HttpStatusCode.OK, "SUCCESS")
        } catch (e: Exceptions.UsernameTakenException) {
            call.respond(HttpStatusCode.Forbidden, "ERR_USERNAME_TAKEN")
        } catch (e: Exceptions.NoSuchExcursionException) {
            call.respond(HttpStatusCode.BadRequest, "ERR_INVALID_EXCURSION")
        }
    }
}




