package net.toomuchram.backendDummy.responders

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.toomuchram.backendDummy.managers.*
import org.joda.time.DateTime
import java.io.ByteArrayOutputStream
import javax.sql.rowset.serial.SerialBlob

class Helpers (private val managers: Managers) {

    /**
     * Function to check if a user is an admin based on their sessionId.
     * Automatically responds to the request if that is not the case.
     * @param call The request
     * @return A boolean indicating whether the user with specified sessionId is an admin or not.
     */
    suspend fun checkAdmin(
        call: ApplicationCall
    ): Boolean {

        try {
            val userId = managers.sessionManager.getUserId(
                getParameters(call)["sessionId"]!!
            )

            if (!managers.userManager.isAdmin(userId)) {
                call.respond(HttpStatusCode.Unauthorized, "ERR_UNAUTHORISED")
                return false
            }
            return true
        } catch (e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
            return false
        } catch (e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
            return false
        }
    }

    /**
     * Function to get the call parameters from any request block
     * This is useful to get the parameters in an intercept block
     * where you don't know the http method
     */
    suspend fun getParameters(call: ApplicationCall): Parameters {
        return when (call.request.httpMethod) {
            HttpMethod.Post -> {
                call.receiveParameters()
            }
            else -> {
                call.request.queryParameters
            }
        }
    }

    // TODO: What the fuck is this?
    /**
     * Function to check if the post parameters contain the correct variables (username + password)
     * @param postParameters The POST parameters
     * @return A pair, with the first value representing if the parameters are correct and the second value containing an optional error message
     */
    fun checkPostParametersForUsernameAndPassword(postParameters: Parameters): Pair<Boolean, String> {
        if (
            !postParameters.contains("username") ||
            !postParameters.contains("password")
        ) {
            return Pair(false, "ERR_MISSING_VARIABLES")
        }

        val username = postParameters["username"]
        val password = postParameters["password"]
        if (username == null || username == "") {
            return Pair(false, "ERR_NO_USERNAME")
        } else if (password == null || password == "") {
            return Pair(false, "ERR_NO_PASSWORD")
        }
        // Second value can be empty as there is no error
        return Pair(true, "")

    }

    /**
     * Function to generate the image path from the chat message, the domain and a sessionId.
     * @param message The chat message containing the image itself and its content type, along with the message ID
     * @param domain The domain on which the application is running
     * @param sessionId A sessionId, needed to generate a valid image URL
     * @param teamId the ID of the team in which the chat message was sent
     * @return A valid image URL
     */
    fun generateImgPath(message: ChatMessage, domain: String, sessionId: String, teamId: String): String {
        return if (message.contentType != null) {
            "https://${domain}/pic?sessionId=${sessionId}&id=${message.id}&teamId=$teamId"
        } else {
            ""
        }
    }

    /**
     * Function to generate a timestmap string from a DateTime object
     * @param timestamp The timestamp
     * @return The timestamp in a string format
     */
    fun timestampToString(timestamp: DateTime): String {
        return timestamp.toLocalDateTime().toString("dd-MM-yyyy HH:mm:ss")
    }

    fun getTeamId(params: Parameters): Int? {
        val tempTeamId = params["teamId"]!!
        return if (tempTeamId == "globalchat") {
            null
        } else {
            tempTeamId.toInt()
        }
    }


    /**
     * Function to extract an image named "img" from a multipart request
     * @param multipart The request
     * @return A pair, containing the image turned into a binary blob and the content type of said image
     */
    // TODO: What the fuck is this?
    suspend fun getImageBlobAndParametersFromMultipart(multipart: MultiPartData): Pair<Parameters, HashMap<String, File>> {
        return withContext(Dispatchers.IO) {


            val fileParameters = HashMap<String, File>()

            val postParameters = Parameters.build {
                multipart.forEachPart { part ->
                    if (part is PartData.FileItem && part.name != null) {
                        val contentType = part.contentType

                        part.streamProvider().use { it ->
                            val output = ByteArrayOutputStream()
                            val buffer = ByteArray(1024)
                            var count: Int
                            while (it.read(buffer).also { count = it } != -1) output.write(buffer, 0, count)
                            val bytes = output.toByteArray()

                            val blob = SerialBlob(bytes)
                            fileParameters[part.name!!] = File(blob, contentType)
                        }
                    } else if(part is PartData.FormItem && part.name != null) {
                        append(part.name!!, part.value)
                    }

                    part.dispose()
                }
            }


            return@withContext Pair(postParameters, fileParameters)
        }

    }

    fun excursionIdFromSession(sessionId: String): Int {
        return managers.userManager.getExcursion(
            managers.sessionManager.getUserId(sessionId)
        )
    }
}