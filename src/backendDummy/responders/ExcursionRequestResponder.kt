package net.toomuchram.backendDummy.responders

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*

fun Routing.excursions(managers: Managers) {
    get("/excursions") {
        val excursions = managers.excursionManager.getAllExcursion()

        call.respond(excursions)
    }
}