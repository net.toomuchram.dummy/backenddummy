package net.toomuchram.backendDummy.responders

import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.request.contentType
import io.ktor.request.receiveMultipart
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondBytes
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import net.toomuchram.backendDummy.managers.*
import net.toomuchram.backendDummy.websocket.WSChatMessage
import org.joda.time.DateTime
import java.lang.NumberFormatException
import java.sql.Blob
import javax.sql.rowset.serial.SerialBlob

fun Route.messaging(
    managers: Managers,
    helpers: Helpers
) {

    post("/sendMessage") {

        val postParameters: Parameters
        var fileParameters = HashMap<String, File>()

        when (call.request.contentType().contentSubtype){
            ContentType.MultiPart.FormData.contentSubtype -> {
                val multipart = call.receiveMultipart()

                val parameters = helpers.getImageBlobAndParametersFromMultipart(multipart)
                postParameters = parameters.first
                fileParameters = parameters.second
            }
            ContentType.Application.FormUrlEncoded.contentSubtype -> {
                postParameters = call.receiveParameters()
            }
            else -> {
                call.respond(HttpStatusCode.BadRequest, "ERR_NO_VARIABLES")
                return@post finish()
            }
        }

        if (!postParameters.contains("sessionId")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_SESSIONID")
            return@post finish()
        }

        val sessionId = postParameters["sessionId"]!!

        // Retrieve the team ID from the POST parameters
        // A team ID of 'globalchat', means we'll have to get the global chat
        // A team ID of 'photoboard' means we'll have to get the photoboard
        // Since the teamID needs to be an integer, map it to -1 and -2 respectively
        val teamId: Int? = if (postParameters.contains("teamId")) {
            try {
                helpers.getTeamId(postParameters)
            } catch (e: NumberFormatException) {
                call.respond(HttpStatusCode.BadRequest, "ERR_INVALID_TEAMID")
                return@post finish()
            }
        } else {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_TEAMID")
            return@post finish()
        }

        val message: String = if (postParameters.contains("message")) {
            postParameters["message"]!!
        } else {
            ""
        }

        val image = if (fileParameters.contains("img")) {
            fileParameters["img"]!!
        } else {
            null
        }
        if (image == null && message == "") {
            call.respond(HttpStatusCode.BadRequest, "ERR_NO_MESSAGE")
            return@post finish()
        }


        try {
            val userId = managers.sessionManager.getUserId(sessionId)
            val excursionId = managers.userManager.getExcursion(userId)
            val username = managers.userManager.getUsername(userId)

            var authorised = false
            if (managers.userManager.isAdmin(userId)) {
                // Admins are allowed to send messages in any chat
                authorised = true
            } else if (teamId == null /* TeamID null means global chat */) {
                // Everyone has the right to send a message in the global chat
                authorised = true
            } else {
                //Check if the user is in the team
                // or if the team exists at all
                try {
                    if (managers.teamsManager.isUserInTeam(userId, teamId)) {
                        authorised = true
                    }
                } catch(e: Exceptions.NoSuchTeamException) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_NO_SUCH_TEAM")
                    return@post finish()
                }
            }

            if (!authorised) {
                call.respond(HttpStatusCode.Unauthorized, "ERR_UNAUTHORISED")
            }

            val imageData = image?.data ?: SerialBlob(byteArrayOf(0))

            val chatMessage = ChatMessage(
                user = username,
                timestamp = DateTime.now(),
                img = imageData,
                contentType = image?.contentType,
                message = message
            )
            val messageWithId = managers.chatManager.addMessage(
                chatMessage,
                teamId,
                excursionId
            )
            val wsMessage = WSChatMessage (
                id = messageWithId.id,
                imgpath = "",
                timestamp = helpers.timestampToString(messageWithId.timestamp),
                user = messageWithId.user,
                message = messageWithId.message,
                channel = "msg",
                teamId = teamId?.toString() ?: "globalchat"
            )

            if (teamId == null) {
                //Send the message to everybody over WS
                managers.websocket.broadcastChatMessage(excursionId) { wsSessionId ->
                    wsMessage.imgpath =
                        helpers.generateImgPath(
                            messageWithId,
                            managers.configManager.readWebserverConfig().domain,
                            wsSessionId,
                            "globalchat"
                        )
                    return@broadcastChatMessage wsMessage
                }
            } else {
                managers.websocket.broadcastChatMessage(excursionId, teamId) { wsSessionId ->
                    wsMessage.imgpath =
                        helpers.generateImgPath(
                            messageWithId,
                            managers.configManager.readWebserverConfig().domain,
                            wsSessionId,
                            teamId.toString()
                        )
                    return@broadcastChatMessage wsMessage
                }
            }

            call.respondText("SUCCESS")

        } catch (e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
        } catch (e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
        } catch (e: Exceptions.NoSuchTeamException) {
            call.respond(HttpStatusCode.BadRequest, "ERR_NO_SUCH_TEAM")
        }
    }

    get("/pic") {
        val queryParameters = call.request.queryParameters
        if (!queryParameters.contains("sessionId")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_SESSIONID")
            return@get finish()
        }
        val sessionId = queryParameters["sessionId"]!!

        val teamId: Int? = if (queryParameters.contains("teamId")) {
            try {
                helpers.getTeamId(queryParameters)
            } catch (e: NumberFormatException) {
                call.respond(HttpStatusCode.BadRequest, "ERR_INVALID_TEAMID")
                return@get finish()
            }
        } else {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_TEAMID")
            return@get finish()
        }

        try {
            val userId = managers.sessionManager.getUserId(sessionId)
            val excursionId = managers.userManager.getExcursion(userId)

            var authorised = false
            if (managers.userManager.isAdmin(userId)) {
                // Admins are allowed to see pictures in any chat
                authorised = true
            } else if (teamId == null) {
                authorised = true
            } else {
                try {
                    if (managers.teamsManager.isUserInTeam(userId, teamId)) {
                        authorised = true
                    }
                } catch(e: Exceptions.NoSuchTeamException) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_NO_SUCH_TEAM")
                    return@get finish()
                }
            }

            if (!authorised) {
                call.respond(HttpStatusCode.Unauthorized, "ERR_UNAUTHORISED")
                return@get finish()
            }

            if (!queryParameters.contains("id")) {
                call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_ID")
                return@get finish()
            }
            val messageId = queryParameters["id"]!!.toInt()

            val msg = managers.chatManager.getMessage(messageId, teamId, excursionId)
            if (msg == null) {
                call.respond(HttpStatusCode.NotFound, "ERR_NO_SUCH_MESSAGE")
                return@get finish()
            }

            val blob = msg.img
            val contentType = msg.contentType
            if (contentType == null) {
                call.respond(HttpStatusCode.NotFound, "ERR_NO_SUCH_PICTURE")
                return@get finish()
            }
            call.respondBytes(blob.getBytes(1, blob.length().toInt()), contentType)
        } catch (e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
        } catch (e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
        }

    }

    get("/getMessages") {
        val queryParameters = call.request.queryParameters
        val sessionId = queryParameters["sessionId"]!!

        // This parameters indicates the batch number of the messages
        // to be sent. A batch consists of 25 messages.
        val batch = if (queryParameters.contains("batch")) {
            queryParameters["batch"]!!.toInt()
        } else {
            0
        }

        val teamId: Int? = if (queryParameters.contains("teamId")) {
            try {
                helpers.getTeamId(queryParameters)
            } catch (e: NumberFormatException) {
                call.respond(HttpStatusCode.BadRequest, "ERR_INVALID_TEAMID")
                return@get finish()
            }
        } else {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_TEAMID")
            return@get finish()
        }

        try {
            val userId = managers.sessionManager.getUserId(sessionId)
            val excursionId = managers.userManager.getExcursion(userId)

            var authorised = false
            if (managers.userManager.isAdmin(userId)) {
                // Admins are allowed to read messages in any chat
                authorised = true
            } else if (teamId == null) {
                authorised = true
            } else {
                try {
                    if (managers.teamsManager.isUserInTeam(userId, teamId)) {
                        authorised = true
                    }
                } catch(e: Exceptions.NoSuchTeamException) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_NO_SUCH_TEAM")
                    return@get finish()
                }
            }

            if (!authorised) {
                call.respond(HttpStatusCode.Unauthorized, "ERR_UNAUTHORISED")
                return@get finish()
            }

            val messages = managers.chatManager.getAllMessages(
                batch = batch,
                teamId = teamId,
                excursionId
            )
            val response = mutableListOf<ChatMessageResponse>()

            for (message in messages) {
                response.add(
                    ChatMessageResponse(
                        id = message.id,
                        imgpath = helpers.generateImgPath(
                            message, managers.configManager.readWebserverConfig().domain, sessionId, teamId?.toString() ?: "globalchat"
                        ),
                        timestamp = helpers.timestampToString(message.timestamp),
                        user = message.user,
                        message = message.message
                    )
                )
            }

            call.respond(response)
        } catch (e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
        } catch (e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
        }
    }
}

data class File (
    val data: Blob,
    val contentType: ContentType?
)
