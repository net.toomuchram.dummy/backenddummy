package net.toomuchram.backendDummy.responders

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.mustache.respondTemplate
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import net.toomuchram.backendDummy.managers.Exceptions
import net.toomuchram.backendDummy.responders.Helpers
import net.toomuchram.backendDummy.responders.Managers

fun Routing.dynamicContent(managers: Managers, helpers: Helpers) {
    get("/dynamicContent") {
        val queryParameters = call.request.queryParameters

        if (!queryParameters.contains("name")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_CONTENT_NAME")
            return@get finish()
        }

        val contentName = queryParameters["name"]!!
        val excursionId = helpers.excursionIdFromSession(queryParameters["sessionId"]!!)

        val content = managers.htmlContentManager.readContent(contentName, excursionId)


        if (content == null) {
            call.respondText("")
        } else {
            call.respondTemplate("htmlcontent.hbs", mapOf(
                "content" to content
            ))
        }
    }

    post("/editDynamicContent") {
        val postParameters = call.receiveParameters()


        if (!postParameters.contains("name")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_CONTENT_NAME")
            return@post finish()
        } else if (!postParameters.contains("content")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_CONTENT")
            return@post finish()
        }

        val sessionId = postParameters["sessionId"]!!

        try {
            val userId = managers.sessionManager.getUserId(sessionId)

            if (!managers.userManager.isAdmin(userId)) {
                call.respond(HttpStatusCode.Unauthorized, "ERR_UNAUTHORISED")
                return@post finish()
            }

            val excursionId = helpers.excursionIdFromSession(sessionId)

            managers.htmlContentManager.insertContent(postParameters["name"]!!, postParameters["content"]!!, excursionId)

            call.respondText("SUCCESS")
        } catch (e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
        } catch (e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
        }

    }
}