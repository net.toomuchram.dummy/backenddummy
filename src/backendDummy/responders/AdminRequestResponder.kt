package net.toomuchram.backendDummy.responders

import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.Parameters
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import net.toomuchram.backendDummy.managers.*
import net.toomuchram.backendDummy.responders.Helpers
import net.toomuchram.backendDummy.responders.Managers

fun Routing.admin(managers: Managers, helpers: Helpers) {

    route("/admin") {
        intercept(ApplicationCallPipeline.Features) {
            val parameters: Parameters = helpers.getParameters(call)
            if(!parameters.contains("sessionId")) {
                call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_SESSIONID")
                return@intercept finish()
            }
            if (helpers.checkAdmin(call)) {
                proceed()
            } else {
                call.respond(HttpStatusCode.Forbidden, "ERR_UNAUTHORISED")
                return@intercept finish()
            }
        }
        // Located here so the userId check is not performed
        get("getUserList") {
            val excursionId = helpers.excursionIdFromSession(call.request.queryParameters["sessionId"]!!)
            call.respond(managers.adminTasks.getAllUsers(excursionId))
        }

        route("users") {
            intercept(ApplicationCallPipeline.Features) {
                val parameters = helpers.getParameters(call)
                if (!parameters.contains("userId")) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_VARIABLES")
                    return@intercept finish()
                }
                proceed()
            }
            post("makeAdmin") {
                val postParameters = call.receiveParameters()
                val excursionId = helpers.excursionIdFromSession(postParameters["sessionId"]!!)
                managers.adminTasks.makeAdmin(postParameters["userId"]!!.toInt(), excursionId)
                call.respondText("SUCCESS")
            }
            post("makeNormal") {
                val postParameters = call.receiveParameters()
                val excursionId = helpers.excursionIdFromSession(postParameters["sessionId"]!!)
                managers.adminTasks.makeNormalUser(postParameters["userId"]!!.toInt(), excursionId)
                call.respondText("SUCCESS")
            }
            post("remove") {
                val postParameters = call.receiveParameters()
                val excursionId = helpers.excursionIdFromSession(postParameters["sessionId"]!!)
                managers.adminTasks.deleteUser(postParameters["userId"]!!.toInt(), excursionId)
                call.respondText("SUCCESS")
            }
            post("changePassword") {
                val postParameters = call.receiveParameters()
                if (!postParameters.contains("newpassword")) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_VARIABLES")
                    return@post finish()
                }
                val newPassword = postParameters["newpassword"]!!
                val excursionId = helpers.excursionIdFromSession(postParameters["sessionId"]!!)

                managers.adminTasks.changePassword(postParameters["userId"]!!.toInt(), newPassword, excursionId)
                call.respondText("SUCCESS")
            }
        }


        route("teams") {
            post("addToTeam") {
                val postParameters = call.receiveParameters()
                if (!postParameters.contains("userId") || !postParameters.contains("teamId")) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_VARIABLES")
                    return@post finish()
                }
                val userId = postParameters["userId"]!!.toInt()
                val teamId = postParameters["teamId"]!!.toInt()

                val adminExcursion = managers.userManager.getExcursion(
                    managers.sessionManager.getUserId(postParameters["sessionId"]!!)
                )

                try {
                    val userExcursion = managers.userManager.getExcursion(userId)
                    managers.teamsManager.addUserToTeam(userId, teamId)

                    if (userExcursion != adminExcursion) {
                        call.respond(HttpStatusCode.Unauthorized, "ERR_INVALID_TEAM")
                    } else {
                        call.respondText("SUCCESS")
                    }
                } catch (e: Exceptions.NoSuchTeamException) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_NO_SUCH_TEAM")
                } catch (e: Exceptions.NoSuchUserException) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_NO_SUCH_USER")
                } catch (e: Exception) {
                    call.respond(HttpStatusCode.InternalServerError, "ERR_INTERNAL")
                    e.printStackTrace()
                }
            }

            post("removeFromTeam") {
                val postParameters = call.receiveParameters()
                if (!postParameters.contains("userId") || !postParameters.contains("teamId")) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_VARIABLES")
                    return@post finish()
                }
                val sessionId = postParameters["sessionId"]!!

                val userId = postParameters["userId"]!!.toInt()
                val teamId = postParameters["teamId"]!!.toInt()

                try {
                    managers.teamsManager.removeUserFromTeam(userId, teamId, helpers.excursionIdFromSession(sessionId))
                    call.respondText("SUCCESS")
                } catch (e: Exceptions.NoSuchTeamException) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_NO_SUCH_TEAM")
                } catch (e: Exception) {
                    call.respond(HttpStatusCode.InternalServerError, "ERR_INTERNAL")
                    e.printStackTrace()
                }

            }

            post("createCategory") {
                val postParameters = call.receiveParameters()
                if (!postParameters.contains("name")) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_VARIABLES")
                    return@post finish()
                }
                val categoryName = postParameters["name"]!!

                val excursionId = helpers.excursionIdFromSession(postParameters["sessionId"]!!)

                try {
                    managers.teamsManager.createTeamCategory(categoryName, excursionId)
                    call.respondText("SUCCESS")
                } catch (e: Exceptions.AlreadyExistsException) {
                    call.respondText("ALREADY_EXISTS")
                } catch (e: Exception) {
                    call.respond(HttpStatusCode.InternalServerError, "ERR_INTERNAL")
                    e.printStackTrace()
                }

            }

            post("createTeam") {
                val postParameters = call.receiveParameters()
                if (!postParameters.contains("name") || !postParameters.contains("categoryId")) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_VARIABLES")
                    return@post finish()
                }

                val sessionId = postParameters["sessionId"]!!

                val teamName = postParameters["name"]!!; val categoryId = postParameters["categoryId"]!!.toInt()
                try {
                    managers.teamsManager.createTeam(teamName, categoryId, helpers.excursionIdFromSession(sessionId))
                    call.respondText("SUCCESS")
                } catch(e: Exceptions.AlreadyExistsException) {
                    call.respondText("ALREADY_EXISTS")
                } catch (e: Exceptions.NoSuchTeamCategoryException) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_NO_SUCH_TEAM_CATEGORY")
                } catch (e: Exception) {
                    call.respond(HttpStatusCode.InternalServerError, "ERR_INTERNAL")
                    e.printStackTrace()
                }
            }

            post("removeCategory") {
                val postParameters = call.receiveParameters()
                if (!postParameters.contains("categoryId")) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_VARIABLES")
                    return@post finish()
                }

                val excursionId = helpers.excursionIdFromSession(
                    postParameters["sessionId"]!!
                )
                val categoryId = postParameters["categoryId"]!!.toInt()
                managers.teamsManager.removeTeamCategory(categoryId, excursionId)
                call.respondText("SUCCESS")
            }

            post("removeTeam") {
                val postParameters = call.receiveParameters()
                if (!postParameters.contains("teamId")) {
                    call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_VARIABLES")
                    return@post finish()
                }
                val excursionId = helpers.excursionIdFromSession(
                    postParameters["sessionId"]!!
                )

                val teamId = postParameters["teamId"]!!.toInt()
                managers.teamsManager.removeTeam(teamId, excursionId)
                call.respondText("SUCCESS")
            }
        }


    }
}