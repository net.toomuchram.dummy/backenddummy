package net.toomuchram.backendDummy.responders

import com.google.gson.Gson
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.post
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.toomuchram.backendDummy.websocket.LocationTracker
import net.toomuchram.backendDummy.managers.Exceptions
import net.toomuchram.backendDummy.responders.Managers
import net.toomuchram.backendDummy.websocket.Location
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

fun Routing.locations(
    locationTracker: LocationTracker,
    managers: Managers
) {
    post("/postLocation") {
        val postParameters = call.receiveParameters()

        val sessionId = postParameters["sessionId"]!!

        if (!postParameters.contains("lat") || !postParameters.contains("lon")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_COORDINATES")
        }
        val lat = postParameters["lat"]!!.toDouble()
        val lon = postParameters["lon"]!!.toDouble()

        try {
            val userId = managers.sessionManager.getUserId(sessionId)
            val username = managers.userManager.getUsername(userId)

            val excursionId = managers.userManager.getExcursion(userId)

            val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
            val location = Location(
                username = username,
                lat = lat,
                lon = lon,
                time = ZonedDateTime.now().toOffsetDateTime().format(formatter)
            )
            locationTracker.addLocation(
                userId,
                excursionId,
                location
            )


            CoroutineScope(Dispatchers.IO).launch {
                val gson = Gson()
                val wsLocationBroadcast = gson.toJson(location)

                managers.websocket.broadcast(wsLocationBroadcast, excursionId)
            }

            call.respondText("SUCCESS")
        } catch (e: Exceptions.NoSuchUserException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_USER")
        } catch (e: Exceptions.NoSuchSessionException) {
            call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
        }
    }
}