package net.toomuchram.backendDummy.responders

data class LoginResponse(
    val username: String,
    val userId: Int,
    val sessionId: String,
    val admin: Int
)

data class ChatMessageResponse(
    val id: Int,
    var imgpath: String,
    val timestamp: String,
    val user: String,
    val message: String
)

data class UserResponse(
    val id: Int,
    val username: String,
    val admin: Boolean
)

data class TeamResponse(
    val id: Int,
    val name: String,
    val categoryId: Int
)

data class TeamCategoryResponse(
    val id: Int,
    val name: String
)

data class TeamMemberResponse(
    val id: Int,
    val name: String
)
