package net.toomuchram.backendDummy.responders

import io.ktor.application.call
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.mustache.MustacheContent
import io.ktor.request.userAgent
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.response.respondText
import io.ktor.routing.Routing
import io.ktor.routing.get
import net.toomuchram.backendDummy.responders.Managers
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

fun Routing.staticRequests(managers: Managers) {

    /**
     * Some hackery is required to get everything to work
     * at the root domain.
     * I settled on a redirect because it was the easiest solution.
     */
    get("/") {
        call.respondRedirect("index.html", true)
    }
    static {
        static("/") {
            resources("static/promotionalsite")
        }
    }

    get("/status") {
        call.respondText("Romereis backend operational at ${managers.configManager.readWebserverConfig().domain}")
    }

    get("/download") {
        val userAgent = call.request.userAgent()
        when {
            Regex("/iPhone|iPad|iPod/i").containsMatchIn(userAgent.toString()) -> {
                call.respondRedirect("https://apps.apple.com/nl/app/romereis-2020/id1482554309")
            }
            Regex("/Android/i").containsMatchIn(userAgent.toString()) -> {
                call.respondRedirect("https://play.google.com/store/apps/details?id=net.toomuchram.dummy")
            }
            else -> {
                call.respondRedirect("/")
            }
        }
    }

    get("/countdown") {
        val timeNow = System.currentTimeMillis()

        //2020-03-07 7:20
        val ldt = LocalDateTime.of(2030, 3, 7, 7, 20, 0)
        val zdt = ldt.atZone(ZoneId.of("Europe/Amsterdam"))
        val millis = zdt.toInstant().toEpochMilli()

        val difference = millis - timeNow
        val daysremaining: Int = if (difference < 0) {
            0
        } else {
            (difference / (1000 * 60 * 60 * 24)).toInt()
        }

        val description = when {
            daysremaining > 1 -> {
                "Nog $daysremaining dagen te gaan"
            }
            daysremaining == 1 -> {
                "Nog $daysremaining dag te gaan"
            }
            else -> {
                ""
            }
        }

        call.respond(
            MustacheContent(
                "countdown.hbs",
                mapOf("description" to description, "date" to zdt.format(DateTimeFormatter.ISO_INSTANT))
            )
        )
    }
}